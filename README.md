# SporifyProxy #

A proxy for spotify, search a term and show the spotify results, with the picture and the title, the audio is unrelated

# Set Up #

- To run, clone this repo

- cd to the folder

- run `npm i`

- and then `npm start`

- enjoy!

# Notes # 

Started as a copy of TransactionTracker, most of the work went into the design of the player, too bad I couldn't get a good audio sample
