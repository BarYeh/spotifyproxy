import '../css/listEntry.css';
import { useEffect, useState } from 'react';
import Player from './Player'

const ListEntry = (props) => {
    const [hovered, setHovered] = useState(false);
    const [width, setWidth] = useState(0);
    useEffect(() => {

    })
    if (props.type === 'artist') {
        return (
            <div className="EntryContainer" onMouseEnter={() => setHovered(true)} onMouseLeave={() => setHovered(false)}>
                {!!props.images && !!props.images.length ? (
                    <div className="Image" style={hovered ? {} : { border: 'solid 3px transparent' }}>
                        <img
                            src={props.album.images[0].url}
                            height={'100vh'}
                            width={'100vh'}
                            className="Picture"
                        />
                    </div>
                ) : (
                    <div style={{ margin: 14, height: '30vh', width: '30vh' }} />
                )}
                <div className="Banner">
                    <div className="DetailsSection">
                        {!!props.name && (
                            <div className="Name">{props.name}</div>
                        )}
                        {!!props.followers && (
                            <div
                                className="Listeners"
                                onClick={() => props.clickLink(null, 0)}
                            >
                                {props.followers.total} listeners
                            </div>
                        )}
                    </div>
                    {!!props.external_urls && (
                        <div
                            className="portion Link"
                            onClick={() => window.open(props.external_urls.spotify, '_blank')}
                        >
                            <div className="LinkHolder">
                                {'>'}
                            </div>
                        </div>
                    )}
                </div>
            </div>
        );
    }
    const picSize = props.screenSize < 800 ? '50vh' : '100vh';
    return (
        <div className="EntryContainer" onMouseEnter={() => setHovered(true)} onMouseLeave={() => setHovered(false)}>
            {!!props.album && !!props.album.images.length && (
                <div className="Image" style={hovered ? {} : { border: 'solid 3px transparent' }}>
                    <img src={props.album.images[0].url} height={picSize} width={picSize} className="Picture" />
                </div>
            )}
            <div className="DetailsSection">
                {!!props.name && (
                    <div className="Name">{props.name}</div>
                )}
                {!!props.artists && !!props.artists.length &&
                    props.artists.map(artist => (
                        <div
                            className="portion From"
                            onClick={() => props.clickAddress(null, 0)}
                        >
                            {artist.name}
                        </div>
                    ))
                }
            </div>
            <Player id={props.id} duration={props.duration_ms} src={null} screenSize={props.screenSize} />
        </div>
    );
}

export default ListEntry;