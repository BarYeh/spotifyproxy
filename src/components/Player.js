import React, { Component, useState, useEffect, useRef } from 'react';
import '../css/Player.css';
import playIcon from '../play.png';
import pauseIcon from '../pause-button.png';

export default props => {
//   playAudio() {
//     const audioEl = document.getElementsByClassName("audio-element")[0]
//     audioEl.play()
//   }
    const [element, setElement] = useState(null);
    const [progress, setProgress] = useState(0);
    const [totalTime, setTotalTime] = useState(60);
    const className = `audio-element-${props.id}`;
    const progressBarRect = useRef({});
    useEffect(() => {
        setElement(document.getElementsByClassName(className)[0]);
    }, []);
    

    useEffect(() => {
        const progressBarElement = document.getElementsByClassName('ProgressBar')[0];
        if (progressBarElement) {
            progressBarRect.current = progressBarElement.getBoundingClientRect();
        }
    }, [progress, element, totalTime]);
    // console.log('progressBarLeft', progressBarLeft);

    const timeUpdate = () => {
        setProgress(element.currentTime);
    }

    const play = () => {
        element.play();
        setTotalTime(element.duration);
    }

    const pause = () => {
        element.pause();
    }

    const playPause = () => {
        if (element.paused) {
            console.log('playing....');
            play();
        } else {
            console.log('pausing....');
            pause();
        }
    }
    // console.log('progress', progress);
    // console.log('totalTime', totalTime);

    const progressBar = () => {
        console.log('progressBar', props.screenSize);
        if (props.screenSize < 800) return null;
        return (
            <div
                className="ProgressBar"
                // style={{ width }}
                onClick={(e) => {
                    const left = e.clientX - progressBarRect.current.left;
                    const width = progressBarRect.current.width;
                    const portion = left / width;
                    const time = portion * totalTime;
                    element.currentTime = time;
                    setProgress(time);
                }}
            >
                <div className="ProgressBarDone" style={{ flex: progress }}/>
                <div style={{ flex: totalTime - progress }}/>
            </div>
        );
    }

    const playButton = () => {
        if (!element) return null;
        const size = 40;
        return (
            <div
                onClick={playPause}
                style={{ width: size, height: size, marginRight: props.screenSize < 700 ? 0 : 20 }}
                className="PlayButton"
            >   
                <div style={{ display: 'flex', justifyContent: 'center' }}>
                    {element.paused ? (
                        <img src={playIcon} height={size/2} width={size/2} style={{ marginTop: size/4, marginLeft: size/8 }} />
                    ) : (
                        <img src={pauseIcon} height={size/2} width={size/2} style={{ marginTop: size/4 }} />
                    )}
                </div>
            </div>
        );
    }
    return (
        <div className="Player" style={{ width: props.screenSize < 700 ? 40 : '75%' }}>
            {playButton()}
            {progressBar()}
            <audio className={className} onTimeUpdate={timeUpdate}>
                <source src="https://www2.cs.uic.edu/~i101/SoundFiles/ImperialMarch60.wav"></source>
            </audio>
        </div>
    )
}