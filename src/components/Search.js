import '../css/mainPage.css';
import React, { useState, useEffect } from 'react';
import Loading from 'react-loading';

const loaderStyle = {
    color: '#68e07c',
    type: "spinningBubbles"
}

const disabledStyle = {
  opacity: 0.3,
  cursor: 'default',
  pointerEvents: 'none'
}

function Search(props) {
  const [address, setAddress] = useState('');
  const { isLoading, onClick, text } = props;

  useEffect(() => {
    setAddress(text);
  }, [text]);

  return (
      <div className="SearchContainer">
        <input
          type="string"
          className="SearchInput"
          value={address}
          onChange={(element) => setAddress(element.target.value)}
        />
        <div className="SearchButtonContainer">
          {isLoading ? (
            <Loading { ...loaderStyle } />
          ) : (
            <div
              className="SearchButtonTextContainer"
              onClick={() => onClick(address, 0)}
            >
              Search
            </div>
          )}
        </div>
      </div>
  );
}

export default Search;