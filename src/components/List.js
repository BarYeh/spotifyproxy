import '../css/listEntry.css';
import { useEffect, useRef, useState } from 'react';
const { default: ListEntry } = require("./ListEntry");

const List = (props) => {
    const columnTitles = {
        name: 'Name'
    }
    const [width, setWidth] = useState(window.innerWidth);
    useEffect(() => {
        window.addEventListener("resize", () => {
            setWidth(window.innerWidth);
        });
    }, []);
    return Array.isArray(props.list) && props.list.length ? (
        <div className="List">
            {/* <ListEntry
                {...columnTitles}
                titles
                clickAddress={props.clickAddress}
            /> */}
            {props.list.map((item) => {
                return <ListEntry key={item.id} { ...item } clickAddress={props.clickLink} screenSize={width} />;
            })}
        </div>
    ) : null;
}

export default List;
