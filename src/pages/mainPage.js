import { getToken, doSearchAPI } from '../APIs/API.js';
import List from '../components/List.js';
import '../css/mainPage.css';
import React, { useEffect, useState } from 'react';
import Search from '../components/Search.js';
import logo from '../logo.png';


function MainPage () {
  const [isLoading, setLoading] = useState(false);
  const [data, setData] = useState({
    artists: {
      items: []
    },
    tracks: {
      items: []
    }
  });
  const [search, setSearch] = useState('');
  const [page, setPage] = useState(0);
  useEffect(() => {
    getToken();
  }, []);

  const doSearch = async (_search, _page) => {
    setLoading(true);
    setSearch(_search);
    setPage(_page);
    const _data = await doSearchAPI(_search);
    console.log(_data);
    setData(_data);
    setLoading(false);
  }

  const _paging = () => {
    const showPrev = page > 0;
    const showNext = data && data.tracks.items.length === 20;
    const style = {
      height: 20,
      width: 20,
    };
    const dummyExtras = {
      paddingLeft: 10
    }
    return (
      <div className="PagingContainer">
          {showPrev
            ? (
              <div
                className="SearchButtonTextContainer"
                onClick={() => doSearch(search, page - 1)}
                style={style}
              >
                {'<'}
              </div>
            ) : <div style={{ ...style, ...dummyExtras }} />
          }
          {showPrev || showNext ? page + 1 : <div />}
          {showNext
            ? (
              <div
                className="SearchButtonTextContainer"
                onClick={() => doSearch(search, page + 1)}
                style={style}
              >
                {'>'}
              </div>
            ) : <div style={{ ...style, ...dummyExtras }} />
          }
      </div>
    )
  }

  return (
    <div className="App">
      <div className="App-header">
        <div className="Acknowlagement">Powered by Spotify API</div>
        {/* <img src={logo} height={60} width={60} style={{ margin: 20 }} /> */}
        <Search
          text={search}
          onClick={doSearch}
          isLoading={isLoading}
        />
        {_paging()}
      </div>
      <List list={data.tracks.items} clickLink={doSearch} />
    </div>
  );
}

export default MainPage;
