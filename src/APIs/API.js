let token;

const accessToken = 'BQCZ4BjBhTbdFOMSXV3iwEALde_o0qeojNh-PSx34U0c92Be0ZggT1iJHqF1XYzbV-GT5mECW82Sr4KoYedLgpWR0PK9qZZzDSL7qCKyUf6vlYg9BZatlBkNLSBYm8CsjTsdU3gRM0S9yMHjXSCeRRhweVi49k0Nl-ukfZBzecw4IHEPFeI-SUY';
const clientId = '4fc9d9d3e5394eaf889222a906d968d5';
const clientSecret = 'c53faac3537645daafb68070e44a8294';

const APICall = (url, method) => {
    return fetch(url, {
        method,
        headers: { 'Authorization' : 'Bearer ' + token}
    });
}

export const getToken = async () => {
    if (token) {
        console.log('okay... but I already have', token);
    }
    const res = await fetch('https://accounts.spotify.com/api/token', {
        method: 'POST',
        headers: {
            'Content-Type' : 'application/x-www-form-urlencoded', 
            'Authorization' : 'Basic ' + btoa(clientId + ':' + clientSecret)
        },
        body: 'grant_type=client_credentials'
    });
    const jsonn = await res.json();
    token = jsonn.access_token;
}

export const doSearchAPI = async (str) => {
    if (!token) {
        console.log('no token :(');
    } else {
        const url = `https://api.spotify.com/v1/search?query=${str}&offset=0&limit=20&type=track`;
        const result = await APICall(url, 'GET');
        const data = await result.json();
        return data;
    }
    return null;
}
